PD=pandoc
PDOPTS=-w beamer --slide-level=3 --template=expose --filter columnfilter.py ${PANDOCOPTS}
LATEX=latexmk
all: pres.pdf
%.tex: %.md
	$(PD) -s $(PDOPTS) -o $@  $<
%.pdf: %.tex
	$(LATEX) -quiet $< || pdflatex -interaction=nonstopmode -file-line-error -shell-escape $<
clean:
	$(LATEX) -c pres
