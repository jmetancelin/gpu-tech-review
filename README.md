---
title: 'Comment aborder les GPU?'
author: Jean-Matthieu Etancelin, LMAP, UPPA
...

Les GPUs sont présents dans de très nombreux systèmes (personnels et mutualisés) à toutes les échelles dans l'écosystème du calcul à hautes performances. La question du coût du portage sur GPU est légitime et il n'existe pas de réponse convenable dans tous les cas. Dans cette présentation, j'essaierai de brosser un portait de ce qu'il est possible de faire, avec quels efforts.
