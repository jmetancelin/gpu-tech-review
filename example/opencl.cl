__kernel void dgemm(__global double* A, __global double *B, __global double* C,
                    double alpha,double beta,
                    int m, int k, int n)
{
    int row = get_global_id(0);
    int col = get_global_id(1);
    if (row < m && col < n) {
        double cij = beta * C[row * n + col] ;
        for (int i = 0; i < k; i++) {
            cij += alpha * A[row * k + i] * B[i * n + col];
        }
        C[row * n + col] = cij;
    }
}
