/* C source code is found in dgemm_example.c */

#define min(x,y) (((x) < (y)) ? (x) : (y))

#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <cuda.h>

__global__ void dgemm(double *A, double *B, double *C,
                      double alpha, double beta,
                      int m, int k, int n)
{
    int row = blockIdx.y * blockDim.y + threadIdx.y;
    int col = blockIdx.x * blockDim.x + threadIdx.x;
    if (row < m && col < n) {
        double cij = beta * C[row * n + col] ;
        for (int i = 0; i < k; i++) {
            cij += alpha * A[row * k + i] * B[i * n + col];
        }
        C[row * n + col] = cij;
    }
}
int main()
{
    double *A, *B, *C;
    int m, n, k, i, j;
    double alpha, beta;

    printf ("\n This example computes real matrix C=alpha*A*B+beta*C using \n"
            " dgemm, where A, B, and  C are matrices and \n"
            " alpha and beta are double precision scalars\n\n");

    m = 8000, k = 6000, n = 8000;
    printf (" Initializing data for matrix multiplication C=A*B for matrix \n"
            " A(%ix%i) and matrix B(%ix%i)\n\n", m, k, k, n);
    alpha = 1.0; beta = 0.0;

    A = (double *)malloc( m*k*sizeof( double ));
    B = (double *)malloc( k*n*sizeof( double ));
    C = (double *)malloc( m*n*sizeof( double ));
    if (A == NULL || B == NULL || C == NULL) {
      printf( "\n ERROR: Can't allocate memory for matrices. Aborting... \n\n");
      free(A);
      free(B);
      free(C);
      return 1;
    }

    printf (" Intializing matrix data \n\n");
    for (i = 0; i < (m*k); i++) {
        A[i] = (double)(i+1);
    }

    for (i = 0; i < (k*n); i++) {
        B[i] = (double)(-i-1);
    }

    for (i = 0; i < (m*n); i++) {
        C[i] = 0.0;
    }

    struct timeval begin, end;


    printf (" Computing matrix product using dgemm \n\n");
    gettimeofday(&begin, 0);
    double *A_d, *B_d, *C_d;
    cudaMalloc((void **)&A_d, m*k*sizeof(double));
    cudaMalloc((void **)&B_d, k*n*sizeof(double));
    cudaMalloc((void **)&C_d, m*n*sizeof(double));
    cudaMemcpy(A_d, A, m*k*sizeof(double), cudaMemcpyDefault);
    cudaMemcpy(B_d, B, k*n*sizeof(double), cudaMemcpyDefault);
    cudaMemcpy(C_d, C, m*n*sizeof(double), cudaMemcpyDefault);
    dim3 block(32,32); dim3 grid(1+(m-1)/block.x,1+(n-1)/block.y);
    dgemm<<<grid,block>>>(A_d, B_d, C_d, alpha, beta, m,k,n);
    cudaMemcpy(C, C_d, m*n*sizeof(double), cudaMemcpyDefault);
    gettimeofday(&end, 0);
    long seconds = end.tv_sec - begin.tv_sec;
    long microseconds = end.tv_usec - begin.tv_usec;
    double elapsed = seconds + microseconds*1e-6;
    printf ("\n Computations completed, in %g s.\n\n", elapsed);

    printf (" Top left corner of matrix A: \n");
    for (i=0; i<min(m,6); i++) {
      for (j=0; j<min(k,6); j++) {
        printf ("%12.0f", A[j+i*k]);
      }
      printf ("\n");
    }

    printf ("\n Top left corner of matrix B: \n");
    for (i=0; i<min(k,6); i++) {
      for (j=0; j<min(n,6); j++) {
        printf ("%12.0f", B[j+i*n]);
      }
      printf ("\n");
    }

    printf ("\n Top left corner of matrix C-A*B: \n");
    for (i=0; i<min(m,6); i++) {
      for (j=0; j<min(n,6); j++) {
          for(int l=0;l<k;l++)
              C[j+i*n] -= A[l+i*k]*B[j+l*n];
        printf ("%12.5G", C[j+i*n]);
      }
      printf ("\n");
    }

    printf ("\n Deallocating memory \n\n");
    /* mkl_free(A); */
    /* mkl_free(B); */
    /* mkl_free(C); */
    free(A);
    free(B);
    free(C);

    printf (" Example completed. \n\n");
    return 0;
}
