/* C source code is found in dgemm_example.c */

#define min(x,y) (((x) < (y)) ? (x) : (y))

#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#define CL_TARGET_OPENCL_VERSION 120
#include <CL/cl.h>
cl_device_id create_device();
cl_program build_program(cl_context ctx, cl_device_id dev, const char* filename);
cl_device_id create_device() {

   cl_platform_id platform;
   cl_device_id dev;
   int err;

   /* Identify a platform */
   err = clGetPlatformIDs(1, &platform, NULL);
   if(err < 0) {
      perror("Couldn't identify a platform");
      exit(1);
   }

   // Access a device
   // GPU
   err = clGetDeviceIDs(platform, CL_DEVICE_TYPE_GPU, 1, &dev, NULL);
   if(err == CL_DEVICE_NOT_FOUND) {
      // CPU
      err = clGetDeviceIDs(platform, CL_DEVICE_TYPE_CPU, 1, &dev, NULL);
   }
   if(err < 0) {
      perror("Couldn't access any devices");
      exit(1);
   }

   return dev;
}





/* Create program from a file and compile it */
cl_program build_program(cl_context ctx, cl_device_id dev, const char* filename) {

   cl_program program;
   FILE *program_handle;
   char *program_buffer, *program_log;
   size_t program_size, log_size;
   int err;

   /* Read program file and place content into buffer */
   program_handle = fopen(filename, "r");
   if(program_handle == NULL) {
      perror("Couldn't find the program file");
      exit(1);
   }
   fseek(program_handle, 0, SEEK_END);
   program_size = ftell(program_handle);
   rewind(program_handle);
   program_buffer = (char*)malloc(program_size + 1);
   program_buffer[program_size] = '\0';
   fread(program_buffer, sizeof(char), program_size, program_handle);
   fclose(program_handle);

   /* Create program from file
   Creates a program from the source code in the add_numbers.cl file.
   Specifically, the code reads the file's content into a char array
   called program_buffer, and then calls clCreateProgramWithSource.
   */
   program = clCreateProgramWithSource(ctx, 1,
      (const char**)&program_buffer, &program_size, &err);
   if(err < 0) {
      perror("Couldn't create the program");
      exit(1);
   }
   free(program_buffer);

   /* Build program
   The fourth parameter accepts options that configure the compilation.
   These are similar to the flags used by gcc. For example, you can
   define a macro with the option -DMACRO=VALUE and turn off optimization
   with -cl-opt-disable.
   */
   err = clBuildProgram(program, 0, NULL, NULL, NULL, NULL);
   if(err < 0) {

      /* Find size of log and print to std output */
      clGetProgramBuildInfo(program, dev, CL_PROGRAM_BUILD_LOG,
            0, NULL, &log_size);
      program_log = (char*) malloc(log_size + 1);
      program_log[log_size] = '\0';
      clGetProgramBuildInfo(program, dev, CL_PROGRAM_BUILD_LOG,
            log_size + 1, program_log, NULL);
      printf("%s\n", program_log);
      free(program_log);
      exit(1);
   }

   return program;
}

int main() {

    /* OpenCL structures */
    cl_device_id device;
    cl_context context;
    cl_program program;
    cl_kernel kernel;
    cl_command_queue queue;
    cl_int err;
    size_t local_size, global_size;

    /* Data and buffers    */
    // Host input and output vectors
    double *A, *B, *C;
    int m, n, k, i, j;
    double alpha, beta;

    m = 8000, k = 6000; n = 8000;
    alpha =1.0, beta=0.0;

    // Device input and output buffers
    cl_mem A_d, B_d, C_d;

    // Allocate host arrays
    A = (double *)malloc( m*k*sizeof( double ));
    B = (double *)malloc( k*n*sizeof( double ));
    C = (double *)malloc( m*n*sizeof( double ));
    if (A == NULL || B == NULL || C == NULL) {
        printf( "\n ERROR: Can't allocate memory for matrices. Aborting... \n\n");
        free(A);
        free(B);
        free(C);
        return 1;
    }

    for (i = 0; i < (m*k); i++) {
        A[i] = (double)(i+1);
    }

    for (i = 0; i < (k*n); i++) {
        B[i] = (double)(-i-1);
    }

    for (i = 0; i < (m*n); i++) {
        C[i] = 0.0;
    }

    struct timeval begin, end;

    device = create_device();
    context = clCreateContext(NULL, 1, &device, NULL, NULL, &err);
    program = build_program(context, device, "opencl.cl");
    queue = clCreateCommandQueue(context, device, 0, &err);
    kernel = clCreateKernel(program, "dgemm", &err);

    gettimeofday(&begin, 0);
    A_d = clCreateBuffer(context, CL_MEM_READ_ONLY, m*k*sizeof( double ), NULL, NULL);
    B_d = clCreateBuffer(context, CL_MEM_READ_ONLY, k*n*sizeof( double ), NULL, NULL);
    C_d = clCreateBuffer(context, CL_MEM_READ_WRITE, m*n*sizeof( double ), NULL, NULL);

    err = clEnqueueWriteBuffer(queue, A_d, CL_TRUE, 0, m*k*sizeof( double ), A, 0, NULL, NULL);
    err = clEnqueueWriteBuffer(queue, B_d, CL_TRUE, 0, k*n*sizeof( double ), B, 0, NULL, NULL);
    err = clEnqueueWriteBuffer(queue, C_d, CL_TRUE, 0, m*n*sizeof( double ), C, 0, NULL, NULL);

    i=0; err =  clSetKernelArg(kernel, i, sizeof(cl_mem), &A_d);
    i++; err |= clSetKernelArg(kernel, i, sizeof(cl_mem), &B_d);
    i++; err |= clSetKernelArg(kernel, i, sizeof(cl_mem), &C_d);
    i++; err |= clSetKernelArg(kernel, i, sizeof(double), &alpha);
    i++; err |= clSetKernelArg(kernel, i, sizeof(double), &beta);
    i++; err |= clSetKernelArg(kernel, i, sizeof(int), &m);
    i++; err |= clSetKernelArg(kernel, i, sizeof(int), &k);
    i++; err |= clSetKernelArg(kernel, i, sizeof(int), &n);

    local_size = 16;
    size_t g_size[2] = {(1+(m-1)/local_size)*local_size,
        (1+(n-1)/local_size)*local_size};
    size_t l_size[2] = {local_size, local_size};
    clFinish(queue);
    err = clEnqueueNDRangeKernel(queue, kernel, 2, NULL, g_size, l_size, 0, NULL, NULL);
    clFinish(queue);
    clEnqueueReadBuffer(queue, C_d, CL_TRUE, 0, m*n*sizeof(double), C, 0, NULL, NULL);
    clFinish(queue);
    gettimeofday(&end, 0);
    long seconds = end.tv_sec - begin.tv_sec;
    long microseconds = end.tv_usec - begin.tv_usec;
    double elapsed = seconds + microseconds*1e-6;
    printf ("\n Computations completed, in %g s.\n\n", elapsed);

    /* Check result */
    printf (" Top left corner of matrix A: \n");
    for (i=0; i<min(m,6); i++) {
        for (j=0; j<min(k,6); j++) {
            printf ("%12.0f", A[j+i*k]);
        }
        printf ("\n");
    }

    printf ("\n Top left corner of matrix B: \n");
    for (i=0; i<min(k,6); i++) {
        for (j=0; j<min(n,6); j++) {
            printf ("%12.0f", B[j+i*n]);
        }
        printf ("\n");
    }

    printf ("\n Top left corner of matrix C-A*B: \n");
    for (i=0; i<min(m,6); i++) {
        for (j=0; j<min(n,6); j++) {
            for(int l=0;l<k;l++)
                C[j+i*n] -= A[l+i*k]*B[j+l*n];
            printf ("%12.5G", C[j+i*n]);
        }
        printf ("\n");
    }

    /* Deallocate resources */
    clReleaseKernel(kernel);
    clReleaseMemObject(A_d);
    clReleaseMemObject(B_d);
    clReleaseMemObject(C_d);
    clReleaseCommandQueue(queue);
    clReleaseProgram(program);
    clReleaseContext(context);
    free(A);
    free(B);
    free(C);
    return 0;
}
