program fortran18
  implicit none
  integer :: m,k,n
  integer :: i,j
  real(kind(1.0d0)) :: alpha, beta
  real(kind(1.0d0)), allocatable :: A(:,:)
  real(kind(1.0d0)), allocatable :: B(:,:)
  real(kind(1.0d0)), allocatable :: C(:,:)
  real :: start, finish

  m = 8000
  k = 6000
  n = 8000
  alpha = 1.0
  beta = 0.0
  allocate(A(m,k))
  allocate(B(k,n))
  allocate(C(m,n))

  do j=1,k
     A(:,j) = (/ (i, i=1+(j-1)*m,j*m) /)
  enddo
  do j=1,n
     B(:,j) = (/ (-i, i=1+(j-1)*k,j*k) /)
  enddo
  C(:,:) = 0.0d0

  call cpu_time(start)
  do concurrent (j=1:n, i=1:m)
     C(i,j) = beta*C(i,j)+alpha*dot_product(A(i,:),B(:,j))
  enddo
  call cpu_time(finish)
  print '("Computations completed, in ",f6.3," s.")',finish-start

  ! C = C - matmul(A,B)

  ! write(*,*) A(:6, :6)
  ! write(*,*) B(:6, :6)
  ! write(*,*) C(:6, :6)

  deallocate(A)
  deallocate(B)
  deallocate(C)

end program fortran18
