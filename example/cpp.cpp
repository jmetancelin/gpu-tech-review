/* C source code is found in dgemm_example.c */


#include <iostream>
#include <sys/time.h>
#include <vector>
#include <algorithm>

using namespace std;

int main()
{
    int m, n, k, i, j;
    double alpha, beta;

    printf ("\n This example computes real matrix C=alpha*A*B+beta*C using \n"
            " dgemm, where A, B, and  C are matrices and \n"
            " alpha and beta are double precision scalars\n\n");

    m = 800, k = 600, n = 800;
    printf (" Initializing data for matrix multiplication C=A*B for matrix \n"
            " A(%ix%i) and matrix B(%ix%i)\n\n", m, k, k, n);
    alpha = 1.0; beta = 0.0;
    vector<double> A(m*k);
    vector<double> B(k*n);
    vector<double> C(m*n);
    if (A.empty() || B.empty() || C.empty()) {
      printf( "\n ERROR: Can't allocate memory for matrices. Aborting... \n\n");
      return 1;
    }

    printf (" Intializing matrix data \n\n");
    for (i = 0; i < (m*k); i++) {
        A[i] = (double)(i+1);
    }

    for (i = 0; i < (k*n); i++) {
        B[i] = (double)(-i-1);
    }

    for (i = 0; i < (m*n); i++) {
        C[i] = 0.0;
    }

    struct timeval begin, end;

    for (const auto& [first, sec] : edges)
    {
        std::cout << first;
    }

    printf (" Computing matrix product \n\n");
    gettimeofday(&begin, 0);
    transform()
    gettimeofday(&end, 0);
    long seconds = end.tv_sec - begin.tv_sec;
    long microseconds = end.tv_usec - begin.tv_usec;
    double elapsed = seconds + microseconds*1e-6;
    printf ("\n Computations completed, in %g s.\n\n", elapsed);

    printf (" Top left corner of matrix A: \n");
    for (i=0; i<min(m,6); i++) {
      for (j=0; j<min(k,6); j++) {
        printf ("%12.0f", A[j+i*k]);
      }
      printf ("\n");
    }

    printf ("\n Top left corner of matrix B: \n");
    for (i=0; i<min(k,6); i++) {
      for (j=0; j<min(n,6); j++) {
        printf ("%12.0f", B[j+i*n]);
      }
      printf ("\n");
    }

    printf ("\n Top left corner of matrix C-A*B: \n");
    for (i=0; i<min(m,6); i++) {
      for (j=0; j<min(n,6); j++) {
          for(int l=0;l<k;l++)
              C[j+i*n] -= A[l+i*k]*B[j+l*n];
        printf ("%12.5G", C[j+i*n]);
      }
      printf ("\n");
    }


    printf (" Example completed. \n\n");
    return 0;
}
