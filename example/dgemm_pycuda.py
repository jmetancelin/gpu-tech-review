import pycuda.driver as cuda
import pycuda.autoinit
from pycuda.compiler import SourceModule
import numpy as np
import math
import timeit

m = 8000
k = 6000
n = 8000
alpha = 1.0
beta = 0.0

A=(np.arange(m*k)+1.0).reshape((m,k))
B=(-np.arange(k*n)-1.0).reshape((k,n))
C=np.ones((m,n), dtype=np.float64)

_dgemm = SourceModule("""
  __global__ void dgemm(double *A, double *B, double *C,
                        double alpha, double beta,
                        int m, int k, int n)
  {
    int row = blockIdx.y * blockDim.y + threadIdx.y;
    int col = blockIdx.x * blockDim.x + threadIdx.x;
    if (row < m && col < n) {
      double cij = beta * C[row * n + col] ;
      for (int i = 0; i < k; i++) {
        cij += alpha * A[row * k + i] * B[i * n + col];
      }
      C[row * n + col] = cij;
    }
  }
  """).get_function("dgemm")

def dgemm(alpha,A,B,beta, C):
    bs=(32,32,1)
    _dgemm(cuda.In(A), cuda.In(B), cuda.InOut(C),
           np.float64(alpha), np.float64(beta),
           np.int32(m), np.int32(k), np.int32(n),
           block=bs, grid=tuple(map(int,np.ceil(np.asarray(A.shape+(1,))/bs).tolist())))


elapsed = timeit.timeit(lambda:dgemm(alpha,A,B,beta, C), number=1)
print(f"Computations completed, in {elapsed} s.");
print(f"Top left A\n{A[:6,:6]}")
print(f"Top left B\n{B[:6,:6]}")
print(f"Top left C-AB\n{C[:6,:6]-(A[:6,:] @ B[:,:6])}")
