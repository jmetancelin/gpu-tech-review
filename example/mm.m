m = 8000; k = 6000; n = 8000;
A = double( rand(m,k) );
B = double( rand(k,n) );
C = double( rand(m,n) );
alpha = 1.0;
beta = 0.0;

start = clock();
C = alpha*A*B+beta*C;
elapsedTime = etime(clock(), start);

disp(sprintf("Computations completed in  %9.6fs ", ...
             elapsedTime) );
