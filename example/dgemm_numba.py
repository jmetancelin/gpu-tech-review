from numba import cuda
import numpy as np
import math
import timeit

@cuda.jit
def matmul(a, A, B, b, C):
    """Perform square matrix multiplication of C = aA*B + bC"""
    i, j = cuda.grid(2)
    if i < C.shape[0] and j < C.shape[1]:
        cij = beta*C[i, j]
        for k in range(A.shape[1]):
            cij += alpha*A[i, k] * B[k, j]
        C[i, j] = cij

m = 8000
k = 6000
n = 8000
alpha = 1.0
beta = 0.0

A=(np.arange(m*k)+1.0).reshape((m,k))
B=(-np.arange(k*n)-1.0).reshape((k,n))
C=np.zeros((m,n), dtype=np.float64)

def dgemm(alpha,A,B,beta, C):
    A_d = cuda.to_device(A)
    B_d = cuda.to_device(B)
    C_d = cuda.to_device(C)
    threadsperblock = (32, 32)
    blockspergrid_x = math.ceil(C.shape[0] / threadsperblock[0])
    blockspergrid_y = math.ceil(C.shape[1] / threadsperblock[1])
    blockspergrid = (blockspergrid_x, blockspergrid_y)
    matmul[blockspergrid, threadsperblock](alpha, A_d, B_d, beta, C_d)
    C[...] = C_d.copy_to_host()

# Warmup (jit compiling)
dgemm(alpha,A,B,beta, C)

elapsed = timeit.timeit(lambda:dgemm(alpha,A,B,beta, C), number=1)
print(f"Computations completed, in {elapsed} s.");
print(f"Top left A\n{A[:6,:6]}")
print(f"Top left B\n{B[:6,:6]}")
print(f"Top left C-AB\n{C[:6,:6]-(A[:6,:] @ B[:,:6])}")
