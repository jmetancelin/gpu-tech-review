import numpy as np
import numpy.linalg as la
import pyopencl as cl
import pyopencl.tools
import pyopencl.array as cl_array

ctx = cl.create_some_context(interactive=False)
queue = cl.CommandQueue(ctx)

import math
import timeit

m = 8000
k = 6000
n = 8000
alpha = 1.0
beta = 0.0

A=(np.arange(m*k)+1.0).reshape((m,k)).astype(np.float64)
B=(-np.arange(k*n)-1.0).reshape((k,n)).astype(np.float64)
C=np.ones((m,n), dtype=np.float64)
prg = cl.Program(ctx, """
  __kernel void dgemm(__global const double *A, __global const double *B, __global double *C,
                        double alpha, double beta,
                        int m, int k, int n)
  {
    int row = get_global_id(1);
    int col = get_global_id(0);
    if (row < m && col < n) {
      double cij = beta * C[row * n + col] ;
      for (int i = 0; i < k; i++) {
        cij += alpha * A[row * k + i] * B[i * n + col];
      }
      C[row * n + col] = cij;
    }
  }
""").build()
def dgemm(alpha,A,B,beta, C):
    bs=(32,32,1)
    A_d = cl_array.to_device(queue, A)
    B_d = cl_array.to_device(queue, B)
    C_d = cl_array.to_device(queue, C)
    wg = (32,32,1)
    gs = tuple(map(int,(np.asarray(wg)*np.ceil(np.asarray(A.shape+(1,))/np.asarray(wg))).tolist()))
    prg.dgemm(queue, gs, wg,
              A_d.data, B_d.data, C_d.data,
              np.float64(alpha), np.float64(beta),
              np.int32(m), np.int32(k), np.int32(n))
    queue.finish()
    C_d.get(queue, C)

elapsed = timeit.timeit(lambda:dgemm(alpha,A,B,beta, C), number=1)
print(f"Computations completed, in {elapsed} s.");
print(f"Top left A\n{A[:6,:6]}")
print(f"Top left B\n{B[:6,:6]}")
print(f"Top left C-AB\n{C[:6,:6]-(A[:6,:] @ B[:,:6])}")
