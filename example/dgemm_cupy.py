import numpy as np
import cupy as cp
import timeit

m = 8000
k = 6000
n = 8000
alpha = 1.0
beta = 0.0

A=(np.arange(m*k)+1.0).reshape((m,k))
B=(-np.arange(k*n)-1.0).reshape((k,n))
C=np.zeros((m,n), dtype=np.float64)

def dgemm(alpha,A,B,beta, C):
    A_d=cp.asarray(A)
    B_d=cp.asarray(B)
    C_d=cp.asarray(C)
    C[...] = (cp.matmul(A_d,B_d)*alpha + C_d*beta).get()

# Warmup (jit compiling)
dgemm(alpha,A,B,beta, C)
elapsed = timeit.timeit(lambda:dgemm(alpha,A,B,beta, C), number=1)
print(f"Computations completed, in {elapsed} s.");
print(f"Top left A\n{A[:6,:6]}")
print(f"Top left B\n{B[:6,:6]}")
print(f"Top left C-AB\n{C[:6,:6]-(A[:6,:] @ B[:,:6])}")
